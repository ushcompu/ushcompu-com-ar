<?php

function mysql_connection() {
  $link = mysqli_connect('localhost', 'username', 'password');
  if (!$link) return false; // array(false, 'Connection failed');
  if (!mysqli_set_charset($link, 'utf8')) return false; //array(false, 'Could not set database character set');
  if (!mysqli_select_db($link, 'database')) { 
    echo 'db selection error:' . mysqli_error($link);
    return false; // array(false, 'Could not select database');
  }
  return $link;
}
