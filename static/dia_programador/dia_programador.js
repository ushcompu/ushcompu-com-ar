// http://ushcompu.com.ar
function ProgrammerDay() {
  this.progDay = 13;
  this.progMonth = 9;
  this.progDate = this.now = new Date();
  this.isNow = false;

  function init() {
    this.now = new Date();
    var day = this.progDay;
    this.isLeap(this.now.getFullYear()) && day--;
    this.progDate = new Date(this.now.getFullYear(), this.progMonth-1, day);
    if (this.now.getMonth() > this.progDate.getMonth() || (this.now.getMonth() == this.progDate.getMonth() && this.now.getDate() > this.progDate.getDate()))
      this.progDate = new Date(this.now.getFullYear()+1, this.progMonth-1, day);

    this.isNow = this.progDate.getFullYear() == this.now.getFullYear() && this.progDate.getMonth() == this.now.getMonth() && this.progDate.getDate() == this.now.getDate();
    this.diff = Math.ceil((this.progDate - this.now)/1000);
  }this.init = init;

  function isLeap(year) {
    return new Date(year,1,29).getDate() == 29;
  }this.isLeap = isLeap;

  function getDiff() {
    diff = this.diff;
    ret = new Array(0,0,0,0);
    ret[3] = diff % 60;
    diff = Math.floor(diff / 60);
    ret[2] = diff % 60;
    diff = Math.floor(diff / 60);
    ret[1] = diff % 24;
    ret[0] = diff = Math.floor(diff / 24);
    if (ret[1] < 10) ret[1] = '0'+ret[1];
    if (ret[2] < 10) ret[2] = '0'+ret[2];
    if (ret[3] < 10) ret[3] = '0'+ret[3];
    return ret;
  }this.getDiff = getDiff;

  function render(id) {
    this.init();
    if (this.isNow) {
      _(id).innerHTML = '¡Es hoy! A brindar \\o/';
    }
    else {
      diff = this.getDiff();
      _(id).innerHTML = 'Faltan<br />Días:horas:minutos:segundos <br />' + diff[0] + ':' + diff[1] + ':' + diff[2] + ':' + diff[3];
    }
  }this.render = render;
}

function main(){
  prog = new ProgrammerDay();
  myInit = function () {prog.render('dias');}
  window.setInterval(myInit, 1000);
}

if (typeof $ == 'undefined') {
  // auxiliar functions
  function _(id) {
    return document.getElementById(id);
  }
  
  if (document.addEventListener)
    document.addEventListener("DOMContentLoaded", main, false)
  else
    window.onload = main;
}
else
  $(function(){ main(); });
